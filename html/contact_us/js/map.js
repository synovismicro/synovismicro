
function processMap() {
  var displaySupportedRegions = false;

  openMap(false);

  jQuery('#usa-back').click(function(e) {        
    openMap(false);
  });      

  jQuery('#show-regions').click(function(e) {
    e.preventDefault();

    displaySupportedRegions = !displaySupportedRegions;
    showSupportedRegions(displaySupportedRegions);
  });

  registerCountryFilter(jQuery('#search_map'), selectCountry);

  function showSupportedRegions(show) {                
    if (show) {                    
      map.setSelectedRegions(getDistributerCodes());          
    } else {
      map.clearSelectedRegions();
    }        
  }

  function openMap(usaMap) {       
    var mapConfig = createMapConfiguration();

    jQuery('#world-map').empty();
    jQuery('#usa-map-content').empty();        
    // Hide any pending tooltips...
    jQuery('.jvectormap-tip').css('display', 'none');
    // Hide any pending distributer infos...
    jQuery('#distributer-info').empty();
    jQuery('#search_map').prop('disabled', usaMap);

    var worldConfig = jQuery.extend({}, mapConfig, { container: jQuery('#world-map') });      
    var usaConfig   = jQuery.extend({}, mapConfig, { container: jQuery('#usa-map-content'), map: 'us_aea_en' });

    var config = usaMap ? usaConfig : worldConfig;        

    jQuery('#world-map').css('display', usaMap ? 'none'  : 'block');        
    jQuery('#usa-map').css(  'display', usaMap ? 'block' : 'none' );

    map = new jvm.Map(config);

    showSupportedRegions(false);

    if (usaMap) {          
      jQuery('.region_toggle').hide();
    } else {
      jQuery('.region_toggle').show();
    }
  }

  function createMapConfiguration() {
    var weights = {};

    if (window.distributes) {
      for (var i=0; i < window.distributes.length; i++) {            
        var distributer = window.distributes[i];

        if (distributer.service_regions && Array.isArray(distributer.service_regions)) {
          for (var j=0; j<distributer.service_regions.length; j++) {                
            weights[distributer.service_regions[j]] = 1;
          }
        }
      }
    }

    jQuery('#world-map path').click(function(e){
      console.log(jQuery(this).attr('data-code'));
    });

    var mapConfig = {
      backgroundColor: '#FFFFFF',
      onRegionClick: onRegionClick,
      onRegionOver: onRegionOver,

      series: {
        regions: [{
          values: weights,
          scale: ['#b0bcbe', '#aaaaaa'],
          normalizeFunction: 'polynomial'
        }]
      },

      regionStyle: {
        initial: {
          "fill": '#bec8ca',
          "fill-opacity": 1,
          "stroke": 'none',
          "stroke-width": 0,
          "stroke-opacity": 1
        },
        hover: {
          "fill-opacity": 0.8,
          "cursor": 'pointer',
          "fill": '#eb1a1e'
        },
        selected: {
          "fill": '#eb1a1e'
        },
        selectedHover: {
        }            
      }
    }

    return mapConfig;
  }

  function onRegionOver(e, code) {
    var distributers = findDistributers(code);

    if (distributers.length === 0) {
      e.preventDefault();
    }
  }

  function onRegionClick(e, code) {        
    // if ('US' == code) {          
    //   openMap(true);
    //   return; 
    // }
    var distributers = findDistributers(code);

    if (distributers.length !== 0) {
      selectCountry(code);
      showDistributers(code);
    }
  }

  function showDistributers(code) {
    var distributers = findDistributers(code);

    jQuery('#distributer-info').empty();

    if (distributers) {
      for (var i=0; i<distributers.length; i++) {
        var distributer = distributers[i];

        var source   = jQuery("#distributer-template").html();
        var template = Handlebars.compile(source);
        var html     = template(distributer);

        jQuery('#distributer-info').append(html);
      }
    }
  }

  function findDistributers(code) {
    if (window.distributes) {

      var distributers = [];

      for (var i=0; i<window.distributes.length; i++) {
        var distributer = window.distributes[i];

        if (distributer.service_regions) {
          for (var j=0; j<distributer.service_regions.length; j++) {
            var serviceRegion = distributer.service_regions[j];

            if (serviceRegion == code) {
              distributers.push(distributer);
              break;
            }
          }
        }            
      }

      return distributers;
    }
  }

  function getDistributerCodes() {
    var codes = [];
    // UK is GB etc - guard agaist be unsupported codes ...
    var unsupportedRegions = ['UK', 'MT', 'SG'];

    if (window.distributes) {
      for (var i=0; i<window.distributes.length; i++) {
        var code = window.distributes[i].country_code;

        if (codes.indexOf(code) < 0) {
          if (unsupportedRegions.indexOf(code) < 0) {
            codes.push(code);
          }              
        }            
      }
    }  

    return codes;      
  }

  function selectCountry(code) {
    map.setFocus({
      region:  code,
      animate: true
    });
    map.clearSelectedRegions();
    map.setSelectedRegions(code);

    showDistributers(code);
  }
}