
window.distributes = [
	{
		country_name: "US",
		country_code: "US",
		region: "",

		service_regions:  ["US"],
		service_regions2: ["US"],

		disptributer_name: "Chris Wright",
		address: "",
		email: "cwright@misonix.com",
		phone: "",
		web_site: "",
		contact: "Vice President of Sales"
	},

	{
		country_name: "China",
		country_code: "CN",
		region: "",

		service_regions:  ["CN"],
		service_regions2: ["CN"],

		disptributer_name: "Dave Battles",
		address: "",
		email: "dbattles@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of Sales, Asia/Pacific"
	},

	{
		country_name: "Australia",
		country_code: "AU",
		region: "",

		service_regions:  ["AU"],
		service_regions2: ["AU"],

		disptributer_name: "Dave Battles",
		address: "",
		email: "dbattles@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of Sales, Asia/Pacific"
	},

	{
		country_name: "New Zealand",
		country_code: "NZ",
		region: "",

		service_regions:  ["NZ"],
		service_regions2: ["NZ"],

		disptributer_name: "Dave Battles",
		address: "",
		email: "dbattles@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of Sales, Asia/Pacific"
	},

	{
		country_name: "Bangladesh",
		country_code: "BD",
		region: "",

		service_regions:  ["BD"],
		service_regions2: ["BD"],

		disptributer_name: "Dave Battles",
		address: "",
		email: "dbattles@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of Sales, Asia/Pacific"
	},
	
	{
		country_name: "India",
		country_code: "IN",
		region: "",

		service_regions:  ["IN"],
		service_regions2: ["IN"],

		disptributer_name: "Dave Battles",
		address: "",
		email: "dbattles@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of Sales, Asia/Pacific"
	},
	
	{
		country_name: "Indonesia",
		country_code: "ID",
		region: "",

		service_regions:  ["ID"],
		service_regions2: ["ID"],

		disptributer_name: "Dave Battles",
		address: "",
		email: "dbattles@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of Sales, Asia/Pacific"
	},
	
	{
		country_name: "Japan",
		country_code: "JP",
		region: "",

		service_regions:  ["JP"],
		service_regions2: ["JP"],

		disptributer_name: "Dave Battles",
		address: "",
		email: "dbattles@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of Sales, Asia/Pacific"
	},

	{
		country_name: "Pakistan",
		country_code: "PK",
		region: "",

		service_regions:  ["PK"],
		service_regions2: ["PK"],

		disptributer_name: "Dave Battles",
		address: "",
		email: "dbattles@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of Sales, Asia/Pacific"
	},

	{
		country_name: "Malaysia",
		country_code: "MY",
		region: "",

		service_regions:  ["MY"],
		service_regions2: ["MY"],

		disptributer_name: "Dave Battles",
		address: "",
		email: "dbattles@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of Sales, Asia/Pacific"
	},	

	{
		country_name: "Singapore",
		country_code: "SG",
		region: "",

		service_regions:  ["SG"],
		service_regions2: ["SG"],

		disptributer_name: "Dave Battles",
		address: "",
		email: "dbattles@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of Sales, Asia/Pacific"
	},

	{
		country_name: "Taiwan",
		country_code: "TW",
		region: "",

		service_regions:  ["TW"],
		service_regions2: ["TW"],

		disptributer_name: "Dave Battles",
		address: "",
		email: "dbattles@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of Sales, Asia/Pacific"
	},

	{
		country_name: "Thailand",
		country_code: "TH",
		region: "",

		service_regions:  ["TH"],
		service_regions2: ["TH"],

		disptributer_name: "Dave Battles",
		address: "",
		email: "dbattles@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of Sales, Asia/Pacific"
	},

	{
		country_name: "Vietnam",
		country_code: "VN",
		region: "",

		service_regions:  ["VN"],
		service_regions2: ["VN"],

		disptributer_name: "Dave Battles",
		address: "",
		email: "dbattles@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of Sales, Asia/Pacific"
	},





 	{
		country_name: "Italy",
		country_code: "IT",
		region: "",

		service_regions:  ["IT"],
		service_regions2: ["IT"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Algeria",
		country_code: "DZ",
		region: "",

		service_regions:  ["DZ"],
		service_regions2: ["DZ"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Estonia",
		country_code: "EE",
		region: "",

		service_regions:  ["EE"],
		service_regions2: ["EE"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	}, 

	{
		country_name: "Latvia",
		country_code: "LV",
		region: "",

		service_regions:  ["LV"],
		service_regions2: ["LV"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Lithuania",
		country_code: "LT",
		region: "",

		service_regions:  ["LT"],
		service_regions2: ["LT"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Belgium",
		country_code: "BE",
		region: "",

		service_regions:  ["BE"],
		service_regions2: ["BE"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Luxembourg",
		country_code: "LU",
		region: "",

		service_regions:  ["LU"],
		service_regions2: ["LU"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Netherlands",
		country_code: "NL",
		region: "",

		service_regions:  ["NL"],
		service_regions2: ["NL"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Czech Republic",
		country_code: "CZ",
		region: "",

		service_regions:  ["CZ"],
		service_regions2: ["CZ"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},
	
	{
		country_name: "Slovakia",
		country_code: "SK",
		region: "",

		service_regions:  ["SK"],
		service_regions2: ["SK"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},
	
	{
		country_name: "Denmark",
		country_code: "DK",
		region: "",

		service_regions:  ["DK"],
		service_regions2: ["DK"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},	

	{
		country_name: "Finland",
		country_code: "FI",
		region: "",

		service_regions:  ["FI"],
		service_regions2: ["FI"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "France",
		country_code: "FR",
		region: "",

		service_regions:  ["FR"],
		service_regions2: ["FR"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Germany",
		country_code: "DE",
		region: "",

		service_regions:  ["DE"],
		service_regions2: ["DE"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Greece",
		country_code: "GR",
		region: "",

		service_regions:  ["GR"],
		service_regions2: ["GR"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Cyprus",
		country_code: "CY",
		region: "",

		service_regions:  ["CY"],
		service_regions2: ["CY"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Israel",
		country_code: "IL",
		region: "",

		service_regions:  ["IL"],
		service_regions2: ["IL"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Saudi Arabia",
		country_code: "SA",
		region: "",

		service_regions:  ["SA"],
		service_regions2: ["SA"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},	

	{
		country_name: "Poland",
		country_code: "PL",
		region: "",

		service_regions:  ["PL"],
		service_regions2: ["PL"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Portugal",
		country_code: "PT",
		region: "",

		service_regions:  ["PT"],
		service_regions2: ["PT"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},	

	{
		country_name: "Russian Federation",
		country_code: "RU",
		region: "",

		service_regions:  ["RU"],
		service_regions2: ["RU"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Belarus",
		country_code: "BY",
		region: "",

		service_regions:  ["BY"],
		service_regions2: ["BY"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},	

	{
		country_name: "Kazakhstan",
		country_code: "KZ",
		region: "",

		service_regions:  ["KZ"],
		service_regions2: ["KZ"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Slovenia",
		country_code: "SI",
		region: "",

		service_regions:  ["SI"],
		service_regions2: ["SI"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},	

	{
		country_name: "Croatia",
		country_code: "HR",
		region: "",

		service_regions:  ["HR"],
		service_regions2: ["HR"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "South Africa",
		country_code: "ZA",
		region: "",

		service_regions:  ["ZA"],
		service_regions2: ["ZA"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},	

	{
		country_name: "Spain",
		country_code: "ES",
		region: "",

		service_regions:  ["ES"],
		service_regions2: ["ES"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Switzerland",
		country_code: "CH",
		region: "",

		service_regions:  ["CH"],
		service_regions2: ["CH"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Turkey",
		country_code: "TR",
		region: "",

		service_regions:  ["TR"],
		service_regions2: ["TR"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "United Kingdom",
		country_code: "GB",
		region: "",

		service_regions:  ["GB"],
		service_regions2: ["GB"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Ireland",
		country_code: "IE",
		region: "",

		service_regions:  ["IE"],
		service_regions2: ["IE"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Canada",
		country_code: "CA",
		region: "",

		service_regions:  ["CA"],
		service_regions2: ["CA"],

		disptributer_name: "Chris Wright",
		address: "",
		email: "cwright@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Vice President of Sales"
	},

	{
		country_name: "Venezuela",
		country_code: "VE",
		region: "",

		service_regions:  ["VE"],
		service_regions2: ["VE"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},	

	{
		country_name: "Mexico",
		country_code: "MX",
		region: "",

		service_regions:  ["MX"],
		service_regions2: ["MX"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Puerto Rico",
		country_code: "PR",
		region: "",

		service_regions:  ["PR"],
		service_regions2: ["PR"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},	

	{
		country_name: "Peru",
		country_code: "PE",
		region: "",

		service_regions:  ["PE"],
		service_regions2: ["PE"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},	

	{
		country_name: "Panama",
		country_code: "PA",
		region: "",

		service_regions:  ["PA"],
		service_regions2: ["PA"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Guatemala",
		country_code: "GT",
		region: "",

		service_regions:  ["GT"],
		service_regions2: ["GT"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},	

	{
		country_name: "Ecuador",
		country_code: "EC",
		region: "",

		service_regions:  ["EC"],
		service_regions2: ["EC"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Colombia",
		country_code: "CO",
		region: "",

		service_regions:  ["CO"],
		service_regions2: ["CO"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Chile",
		country_code: "CL",
		region: "",

		service_regions:  ["CL"],
		service_regions2: ["CL"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Brazil",
		country_code: "BR",
		region: "",

		service_regions:  ["BR"],
		service_regions2: ["BR"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},

	{
		country_name: "Argentina",
		country_code: "AR",
		region: "",

		service_regions:  ["AR"],
		service_regions2: ["AR"],

		disptributer_name: "Michael Barbe",
		address: "",
		email: "mbarbe@misonix.com",
		phone: "",
		fax: "",
		web_site: "",
		contact: "Director of EMEA"
	},
];

